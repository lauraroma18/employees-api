// Package main Employees API.
//
// The purpose of this application is to provide service for create, update and consult employees
//
//
// This should show the struct of endpoints
// Terms Of Service:
//
//     Schemes: http, https
//     Host: localhost:8888
//     BasePath: /api/v1
//     Version: 1.0.0
//     Contact: https://www.linkedin.com/in/lauraroma18/
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
// swagger:meta
package main

import (
	"employees-api/infrastructure"
	_ "github.com/joho/godotenv/autoload"
	"log"
	"os"
)

func main() {
	log.Println("starting API")
	port := os.Getenv("API_PORT")

	infrastructure.Start(port)
}