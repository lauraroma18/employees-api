module employees-api

go 1.15

require (
	github.com/badoux/checkmail v1.2.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dimiro1/health v0.0.0-20191019130555-c5cbb4d46ffc
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/cors v1.2.0
	github.com/go-openapi/analysis v0.20.1 // indirect
	github.com/go-redis/redis/v8 v8.8.0
	github.com/go-swagger/go-swagger v0.27.0 // indirect
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/google/uuid v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.10.0
	github.com/pelletier/go-toml v1.9.0 // indirect
	github.com/satori/go.uuid v1.2.0
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
	golang.org/x/net v0.0.0-20210410081132-afb366fc7cd1 // indirect
	golang.org/x/oauth2 v0.0.0-20210402161424-2e8d93401602
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
)
