package model

import (
	"regexp"
	"strings"
	"time"
)

const dateLayout = "2006-01-02"

// Data of Employee
// swagger:model
type Employee struct {
	ID				string		`json:"id,omitempty"`
	FirstName		string		`json:"first_name,omitempty"`
	OtherName    	string     	`json:"other_name"`
	FirstSurname	string		`json:"first_surname,omitempty"`
	SecondSurname 	string	    `json:"second_surname,omitempty"`
	JobCountry		string		`json:"job_country,omitempty"`
	TypeID        	string     	`json:"type_ID,omitempty"`
	NumberID       	string		`json:"number_ID,omitempty"`
	Email           string      `json:"email,omitempty"`
	EntryDate       string      `json:"entry_date,omitempty"`
	Area            string      `json:"area,omitempty"`
	Status          string      `json:"status,omitempty"`
	CreatedAt       time.Time   `json:"created_at,omitempty"`
	UpdatedAt       time.Time   `json:"updated_at,omitempty"`
}

// Information for update employee
// swagger:parameters updateEmployeeRequest
type SwaggerUpdateEmployeeRequest struct {
	// in: path
	// Required: true
	EmployeeID string

	// in: body
	Body struct {
		// Required: true
		FirstName		string		`json:"first_name,omitempty"`

		OtherName    	string     	`json:"other_name"`
		// Required: true
		FirstSurname	string		`json:"first_surname,omitempty"`

		SecondSurname 	string	    `json:"second_surname,omitempty"`
		// Required: true
		JobCountry		string		`json:"job_country,omitempty"`
		// Required: true
		TypeID        	string     	`json:"type_ID,omitempty"`
		// Required: true
		NumberID       	string		`json:"number_ID,omitempty"`
		// Required: true
		Status          string      `json:"status,omitempty"`
		// Required: true
		Area            string      `json:"area,omitempty"`
	}
}

// Information new employee
// swagger:parameters createEmployeeRequest
type SwaggerCreateEmployeeRequest struct {
	// in: body
	Body struct {
		// Required: true
		FirstName		string		`json:"first_name,omitempty"`

		OtherName    	string     	`json:"other_name"`
		// Required: true
		FirstSurname	string		`json:"first_surname,omitempty"`

		SecondSurname 	string	    `json:"second_surname,omitempty"`
		// Required: true
		JobCountry		string		`json:"job_country,omitempty"`
		// Required: true
		TypeID        	string     	`json:"type_ID,omitempty"`
		// Required: true
		NumberID       	string		`json:"number_ID,omitempty"`
		// Required: true
		EntryDate       string      `json:"entry_date,omitempty"`
		// Required: true
		Area            string      `json:"area,omitempty"`
	}
}

// swagger:parameters getOneEmployeeRequest
type SwaggerGetOneEmployeeRequest struct {
	// in: path
	// Required: true
	ID string
}

// Validate validate the request body
func (e *Employee) Validate(action string) map[string]string {
	e.FirstName = strings.ToUpper(e.FirstName)
	e.OtherName = strings.ToUpper(e.OtherName)
	e.FirstSurname = strings.ToUpper(e.FirstSurname)
	e.SecondSurname = strings.ToUpper(e.SecondSurname)

	var errorMessages = make(map[string]string)

	switch strings.ToLower(action) {
	case "update":
		// first surname
		if e.FirstSurname == "" {
			errorMessages["first_surname_required"] = "Es requerido el primer apellido"
		}

		if !isLetter(strings.ToUpper(e.FirstSurname), "") {
			errorMessages["first_surname_format"] = "Solo se permiten caracteres A-Z"
		}

		if len(e.FirstSurname) > 20 {
			errorMessages["first_surname_len"] = "Debe contener 20 o menos caracteres"
		}

		// second surname
		if e. SecondSurname != "" && !isLetter(strings.ToUpper(e.FirstName), "space") {
			errorMessages["second_surname_format"] = "Solo se permiten caracteres A-Z"
		}

		if len(e.FirstName) > 20 {
			errorMessages["second_surname_len"] = "Debe contener 20 o menos caracteres"
		}

		// first name
		if e.FirstName == "" {
			errorMessages["first_name_required"] = "Es requerido el primer nombre"
		}

		if !isLetter(strings.ToUpper(e.FirstName), "") {
			errorMessages["first_name_format"] = "Solo se permiten caracteres A-Z"
		}

		if len(e.FirstName) > 20 {
			errorMessages["first_name_len"] = "Debe contener 20 o menos caracteres"
		}

		// other names
		if e.OtherName != "" && !isLetter(strings.ToUpper(e.OtherName), "space") {
			errorMessages["other_name_format"] = "Solo se permiten caracteres A-Z"
		}

		if e.OtherName != "" && len(e.OtherName) > 50 {
			errorMessages["other_name_len"] = "Debe contener 50 o menos caracteres"
		}

		// job country
		if e.JobCountry == ""{
			errorMessages["job_country_required"] = "Es requerido el país de empleo"
		}

		// type ID
		if e.TypeID == ""{
			errorMessages["type_ID_required"] = "Es requerido el tipo de identificación"
		}

		// number ID
		if e.NumberID == ""{
			errorMessages["number_ID_required"] = "Es requerido el número de identificación"
		}

		if isLetter(e.NumberID, "id"){
			errorMessages["number_ID_format"] = "Solo se admiten letras y el caracter -"
		}

		// area
		if e.Area == ""{
			errorMessages["area_required"] = "Es requerida un area"
		}

		// status
		if e.Status == ""{
			errorMessages["status_required"] = "Es requerida un estado"
		}

	default:
		// first surname
		if e.FirstSurname == "" {
			errorMessages["first_surname_required"] = "Es requerido el primer apellido"
		}

		if !isLetter(strings.ToUpper(e.FirstSurname), "") {
			errorMessages["first_surname_format"] = "Solo se permiten caracteres A-Z"
		}

		if len(e.FirstSurname) > 20 {
			errorMessages["first_surname_len"] = "Debe contener 20 o menos caracteres"
		}

		// second surname
		if e. SecondSurname != "" && !isLetter(strings.ToUpper(e.FirstName), "space") {
			errorMessages["second_surname_format"] = "Solo se permiten caracteres A-Z"
		}

		if len(e.FirstName) > 20 {
			errorMessages["second_surname_len"] = "Debe contener 20 o menos caracteres"
		}

		// first name
		if e.FirstName == "" {
			errorMessages["first_name_required"] = "Es requerido el primer nombre"
		}

		if !isLetter(strings.ToUpper(e.FirstName), "") {
			errorMessages["first_name_format"] = "Solo se permiten caracteres A-Z"
		}

		if len(e.FirstName) > 20 {
			errorMessages["first_name_len"] = "Debe contener 20 o menos caracteres"
		}

		// other names
		if e.OtherName != "" && !isLetter(strings.ToUpper(e.OtherName), "space") {
			errorMessages["other_name_format"] = "Solo se permiten caracteres A-Z"
		}

		if e.OtherName != "" && len(e.OtherName) > 50 {
			errorMessages["other_name_len"] = "Debe contener 50 o menos caracteres"
		}

		// job country
		if e.JobCountry == ""{
			errorMessages["job_country_required"] = "Es requerido el país de empleo"
		}

		// type ID
		if e.TypeID == ""{
			errorMessages["type_ID_required"] = "Es requerido el tipo de identificación"
		}

		// number ID
		if e.NumberID == ""{
			errorMessages["number_ID_required"] = "Es requerido el número de identificación"
		}

		if isLetter(e.NumberID, "id"){
			errorMessages["number_ID_format"] = "Solo se admiten letras y el caracter -"
		}

		// area
		if e.Area == ""{
			errorMessages["area_required"] = "Es requerida un area"
		}

		// entry date
		currentTime, _ := time.Parse(dateLayout, time.Now().Format(dateLayout))
		if e.EntryDate == ""{
			errorMessages["entry_date_required"] = "Es requerida una fecha de ingreso"
		}else{
			tempDate, err := time.Parse(dateLayout, e.EntryDate)
			if err != nil {
				errorMessages["entry_date_format"] = "Fecha de ingreso tienen un formato incorrecto"
			}else {
				oneMonthAgo := currentTime.Add(30 * 24 * time.Hour * -1)
				if tempDate.Before(oneMonthAgo){
					errorMessages["entry_date_wrong"] = "Fecha de ingreso puede ser hasta un mes menor"
				}

				if tempDate.After(currentTime){
					errorMessages["entry_date_wrong"] = "Fecha de ingreso no puede ser superior a la fecha actual"
				}
			}
		}


	}

	return errorMessages
}

// isLetter validate if an string has right format for an specific case
func isLetter(text string, option string) bool {
	expresion := regexp.MustCompile("^[A-Z]*$")

	if option == "space" {
		expresion = regexp.MustCompile("^[A-Z ]*$")
	}

	if option == "id" {
		expresion = regexp.MustCompile("^[a-zA-Z-]*$")
	}

	if !expresion.MatchString(text) {
		return false
	}

	return true
}

