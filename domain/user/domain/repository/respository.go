package repository

import (
	"context"
	"employees-api/domain/user/application/v1/response"
	"employees-api/domain/user/domain/model"
)

type EmployeeRepository interface {
	CreateEmployee(ctx context.Context, employee *model.Employee) (*response.UserResponse, error)
	AutogenerateEmail(ctx context.Context, employee *model.Employee) (string, error)
	GetEmployeeByID(ctx context.Context, employeeID string) (*response.EmployeeResponse, error)
	UpdateEmployee(ctx context.Context, employee *model.Employee) (*response.UserResponse, error)
	GetAllEmployees(ctx context.Context) ([]response.EmployeeResponse, error)
}