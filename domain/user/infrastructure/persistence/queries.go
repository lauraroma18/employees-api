package persistence

const(

	// insertUser is a query that inserts a new row in the employee table
	insertUser = "INSERT INTO public.employee\n(id, first_name, other_name, first_surname, second_surname, job_country, \"type_ID\", \"number_ID\", email, entry_date, \"area\", status, created_at, updated_at)\nVALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14) RETURNING id, first_name, first_surname, email ;\n"

	// selectCountSimilarEmail return amount the employees with a similar email
	selectCountSimilarEmail = "select count(*) \n\t  from employee e \nwhere e.first_name = $1 and e.first_surname = $2"

	// selectAllEmployees is a query that selects all rows in the employees table
	selectAllEmployees = "SELECT * \nFROM \"employee\" \nWHERE status='activo' \nORDER BY created_at DESC;"

	// selectEmployeeById is a query that selects a row from the employee table by id given
	selectEmployeeById = "select *\n\t  from employee e \nwhere e.id = $1"

	// updateEmployee is a query that updates a row in the employee table based off of id.
	updateEmployee = "UPDATE public.employee\nSET first_name= $1,\n    other_name= $2, \n    first_surname= $3, \n    second_surname= $4, \n    job_country= $5, \n    \"type_ID\"= $6, \n    \"number_ID\"= $7, \n    email= $8, \n    area= $9, \n    status= $10,  \n    updated_at= $11\nWHERE id= $12 RETURNING id, first_name, first_surname, email;"

)
