package persistence

import (
	"context"
	"employees-api/domain/user/application/v1/response"
	"employees-api/domain/user/domain/model"
	repoDomain "employees-api/domain/user/domain/repository"
	"employees-api/infrastructure/database"
	"github.com/satori/go.uuid"
	"strconv"
	"strings"
)


type sqlEmployeeRepo struct {
	Conn *database.Data
}

func NewUserRepository(Conn *database.Data) repoDomain.EmployeeRepository {
	return &sqlEmployeeRepo{
		Conn: Conn,
	}
}

// CreateEmployee insert a new employee in db
func (e *sqlEmployeeRepo) CreateEmployee(ctx context.Context, employee *model.Employee) (*response.UserResponse, error) {
	stmt, err := e.Conn.DB.PrepareContext(ctx, insertUser)
	if err != nil {
		return &response.UserResponse{}, err
	}

	defer stmt.Close()
	if strings.TrimSpace(employee.ID) == "" {
		employee.ID = uuid.NewV4().String()
	}

	row := stmt.QueryRowContext(ctx,
		&employee.ID,
		&employee.FirstName,
		&employee.OtherName,
		&employee.FirstSurname,
		&employee.SecondSurname,
		&employee.JobCountry,
		&employee.TypeID,
		&employee.NumberID,
		&employee.Email,
		&employee.EntryDate,
		&employee.Area,
		&employee.Status,
		&employee.CreatedAt,
		&employee.UpdatedAt)

	employeeResult := response.UserResponse{}
	err = row.Scan(&employeeResult.ID, &employeeResult.FirstName, &employeeResult.FirstSurname, &employeeResult.Email)
	if err != nil {
		return &response.UserResponse{}, err
	}

	return &employeeResult, nil
}

// AutogenerateEmail autogenerate employee email
func (e *sqlEmployeeRepo) AutogenerateEmail(ctx context.Context, employee *model.Employee) (string, error) {
	email := strings.ToLower(employee.FirstName) + "." + strings.ToLower(employee.FirstSurname)

	sequence, err := e.getAmountSimilarEmail(ctx, employee.FirstName, employee.FirstSurname)
	if  err != nil{
		return "", err
	}

	if sequence > 0{
		sequence += 1
		email = email + "." + strconv.FormatInt(sequence, 10)
	}

	if employee.JobCountry == "Colombia"{
	 	email = email + "@cidenet.com.co"
	}

	if employee.JobCountry == "Estados Unidos"{
		email = email + "@cidenet.com.us"
	}

	return email, nil
}

// getAmountSimilarEmail get amount the employees with similar email
func (e *sqlEmployeeRepo) getAmountSimilarEmail(ctx context.Context, name string, surname string) (int64, error){
	stmt, err := e.Conn.DB.PrepareContext(ctx, selectCountSimilarEmail)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()

	row := stmt.QueryRowContext(ctx, name, surname)

	var count int64
	err = row.Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

// GetEmployeeByID get one employee by id given
func (e *sqlEmployeeRepo) GetEmployeeByID(ctx context.Context, employeeID string) (*response.EmployeeResponse, error){
	stmt, err := e.Conn.DB.PrepareContext(ctx, selectEmployeeById)
	if err != nil {
		return &response.EmployeeResponse{}, err
	}
	defer stmt.Close()

	row := stmt.QueryRowContext(ctx, employeeID)

	var result response.EmployeeResponse
	err = row.Scan(&result.ID,
		           &result.FirstName,
		           &result.OtherName,
		           &result.FirstSurname,
		           &result.SecondSurname,
		           &result.JobCountry,
		           &result.TypeID,
		           &result.NumberID,
		           &result.Email,
		           &result.EntryDate,
		           &result.Area,
		           &result.Status,
		           &result.CreatedAt,
		           &result.UpdatedAt)
	if err != nil {
		return &response.EmployeeResponse{}, err
	}

	return &result, nil
}

// UpdateEmployee update employee data
func (e *sqlEmployeeRepo) UpdateEmployee(ctx context.Context, employee *model.Employee) (*response.UserResponse, error) {
	stmt, err := e.Conn.DB.PrepareContext(ctx, updateEmployee)
	if err != nil {
		return &response.UserResponse{}, err
	}

	defer stmt.Close()

	row := stmt.QueryRowContext(ctx,
		&employee.FirstName,
		&employee.OtherName,
		&employee.FirstSurname,
		&employee.SecondSurname,
		&employee.JobCountry,
		&employee.TypeID,
		&employee.NumberID,
		&employee.Email,
		&employee.Area,
		&employee.Status,
		&employee.UpdatedAt,
		&employee.ID)

	employeeResult := response.UserResponse{}
	err = row.Scan(&employeeResult.ID, &employeeResult.FirstName, &employeeResult.FirstSurname, &employeeResult.Email)
	if err != nil {
		return &response.UserResponse{}, err
	}

	return &employeeResult, nil
}

// GetAllEmployees return all records from employee table
func (e *sqlEmployeeRepo) GetAllEmployees(ctx context.Context) ([]response.EmployeeResponse, error) {

	rows, err := e.Conn.DB.QueryContext(ctx, selectAllEmployees)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var employees []response.EmployeeResponse

	for rows.Next() {
		var employeeRow response.EmployeeResponse
		_ = rows.Scan(&employeeRow.ID,
			          &employeeRow.FirstName,
			          &employeeRow.OtherName,
			          &employeeRow.FirstSurname,
			          &employeeRow.SecondSurname,
			          &employeeRow.JobCountry,
			          &employeeRow.TypeID,
			          &employeeRow.NumberID,
			          &employeeRow.Email,
			          &employeeRow.EntryDate,
			          &employeeRow.Area,
			          &employeeRow.Status,
			          &employeeRow.CreatedAt,
					  &employeeRow.UpdatedAt)

		employees = append(employees, employeeRow)
	}

	return employees, nil
}
