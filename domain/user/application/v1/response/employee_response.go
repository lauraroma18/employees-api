package response

import "time"

type UserResponse struct {
	ID           string     `json:"id,omitempty"`
	FirstName    string     `json:"first_name,omitempty"`
	FirstSurname string     `json:"first_surname,omitempty"`
	Email        string     `json:"email,omitempty"`
}

type EmployeeResponse struct {
	ID				string		`json:"id,omitempty"`
	FirstName		string		`json:"first_name,omitempty"`
	OtherName    	string     	`json:"other_name"`
	FirstSurname	string		`json:"first_surname,omitempty"`
	SecondSurname 	string	    `json:"second_surname"`
	JobCountry		string		`json:"job_country,omitempty"`
	TypeID        	string     	`json:"type_ID,omitempty"`
	NumberID       	string		`json:"number_ID,omitempty"`
	Email           string      `json:"email,omitempty"`
	EntryDate       string      `json:"entry_date,omitempty"`
	Area            string      `json:"area,omitempty"`
	Status          string      `json:"status,omitempty"`
	CreatedAt       time.Time   `json:"created_at,omitempty"`
	UpdatedAt       time.Time   `json:"updated_at,omitempty"`
}

// EmployeeResponse contains all employee data
// swagger:response SwaggerAllEmployeeResponse
type SwaggerAllEmployeeResponse struct {
	//in: body
	Body []EmployeeResponse
}

// EmployeeResponse contains all employee data
// swagger:response SwaggerEmployeeResponse
type SwaggerEmployeeResponse struct {
	//in: body
	Body EmployeeResponse
}


// SwaggerBasicEmployeeData contains basic data about employee
// swagger:response SwaggerBasicEmployeeData
type SwaggerBasicEmployeeData struct {
	//in: body
	Body UserResponse
}
