package v1

import (
	"employees-api/domain/user/domain/model"
	repoDomain "employees-api/domain/user/domain/repository"
	"employees-api/domain/user/infrastructure/persistence"
	"employees-api/infrastructure/database"
	"employees-api/infrastructure/middleware"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
	"strings"
	"time"
)


// EmployeeRouter
type EmployeeRouter struct {
	Repo repoDomain.EmployeeRepository
}

// NewUserHandler
func NewEmployeeHandler(db *database.Data) *EmployeeRouter {
	return &EmployeeRouter{
		Repo: persistence.NewUserRepository(db),
	}
}

// swagger:route POST /employees  Employee createEmployeeRequest
//
// CreateEmployeeHandler.
// Create a new employee
//
//
//     consumes:
//     - application/json
//
//     produces:
//      - application/json
//
//	   schemes: http, https
//
//     responses:
//        201: SwaggerBasicEmployeeData
//		  400: SwaggerErrorMessage
//		  409: SwaggerErrorMessage
//		  422: SwaggerErrorsMessage
//		  422: SwaggerErrorsMessage
//
// CreateEmployeeHandler Create a new employee.
func (ur *EmployeeRouter) CreateEmployeeHandler(w http.ResponseWriter, r *http.Request) {
	var employee model.Employee

	err := json.NewDecoder(r.Body).Decode(&employee)
	if err != nil {
		_ = middleware.HTTPError(w, r, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	userErrors := employee.Validate("")
	if len(userErrors) > 0 {
		_ = middleware.HTTPErrors(w, r, http.StatusUnprocessableEntity, userErrors)
		return
	}

	ctx := r.Context()
	employee.Status = "activo"
	employee.CreatedAt = time.Now()
	employee.UpdatedAt = time.Now()

	employee.Email, err = ur.Repo.AutogenerateEmail(ctx, &employee)
	if err != nil {
		_ = middleware.HTTPError(w, r, http.StatusConflict, err.Error())
		return
	}

	result, err := ur.Repo.CreateEmployee(ctx, &employee)
	if err != nil {
		_ = middleware.HTTPError(w, r, http.StatusConflict, err.Error())
		return
	}

	w.Header().Add("Location", fmt.Sprintf("%s%s", r.URL.String(), result.ID))
	_ = middleware.JSON(w, r, http.StatusCreated, result)
}

// swagger:route GET /employees/{employee_id}  Employee getOneEmployeeRequest
//
// GetOneEmployeeHandler.
// Return one employee by id
//
//
//     produces:
//      - application/json
//
//	   schemes: http, https
//
//     responses:
//        200: SwaggerEmployeeResponse
//        204: SwaggerSuccessfullyMessage
//		  400: SwaggerErrorMessage
//		  409: SwaggerErrorMessage
//		  422: SwaggerErrorsMessage
//
// GetOneEmployeeHandler Return one employee by id.
func (ur *EmployeeRouter) GetOneEmployeeHandler(w http.ResponseWriter, r *http.Request) {
	employeeID := chi.URLParam(r, "employee_id")
	if employeeID == "" {
		_ = middleware.HTTPError(w, r, http.StatusBadRequest, errors.New("no se pudo obtener el id del empleado").Error())
		return
	}

	ctx := r.Context()
	userResult, err := ur.Repo.GetEmployeeByID(ctx, employeeID)
	if userResult.ID ==  ""{
		_ = middleware.JSONMessages(w, r, http.StatusNoContent, "No se encontró el empleado")
		return
	}

	if err != nil {
		_ = middleware.HTTPError(w, r, http.StatusConflict, err.Error())
		return
	}

	_ = middleware.JSON(w, r, http.StatusOK, userResult)
}

// swagger:route PATCH /employees/{employee_id}  Employee updateEmployeeRequest
//
// UpdateEmployeeHandler.
// Update the employee by one id given
//
//     consumes:
//     - application/json
//
//     produces:
//      - application/json
//
//	   schemes: http, https
//
//     responses:
//        200: SwaggerBasicEmployeeData
//		  400: SwaggerErrorMessage
//		  409: SwaggerErrorMessage
//		  422: SwaggerErrorsMessage
//
// UpdateEmployeeHandler update an employee
func (ur *EmployeeRouter) UpdateEmployeeHandler(w http.ResponseWriter, r *http.Request) {
	var employee model.Employee
	employeeID := chi.URLParam(r, "employee_id")

	err := json.NewDecoder(r.Body).Decode(&employee)
	if err != nil {
		_ = middleware.HTTPError(w, r, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	userErrors := employee.Validate("update")
	if len(userErrors) > 0 {
		_ = middleware.HTTPErrors(w, r, http.StatusUnprocessableEntity, userErrors)
		return
	}

	ctx := r.Context()
	employee.ID = employeeID
	employee.FirstName = strings.ToUpper(employee.FirstName)
	employee.OtherName = strings.ToUpper(employee.OtherName)
	employee.FirstSurname = strings.ToUpper(employee.FirstSurname)
	employee.SecondSurname = strings.ToUpper(employee.SecondSurname)
	employee.UpdatedAt = time.Now()

	employee.Email, err = ur.Repo.AutogenerateEmail(ctx, &employee)
	if err != nil {
		_ = middleware.HTTPError(w, r, http.StatusConflict, err.Error())
		return
	}

	result, err := ur.Repo.UpdateEmployee(ctx, &employee)
	if err != nil {
		_ = middleware.HTTPError(w, r, http.StatusConflict, err.Error())
		return
	}

	w.Header().Add("Location", fmt.Sprintf("%s%s", r.URL.String(), result.ID))
	_ = middleware.JSON(w, r, http.StatusOK, result)
}

// swagger:route GET /employees  Employee getAllEmployees
//
// GetAllEmployeesHandler.
// Return all employees in the database
//
//     produces:
//      - application/json
//
//	   schemes: http, https
//
//     responses:
//        200: SwaggerAllEmployeeResponse
//		  204: SwaggerSuccessfullyMessage
//		  409: SwaggerErrorMessage
//
// GetAllEmployeesHandler get all employees table
func (ur *EmployeeRouter) GetAllEmployeesHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	users, err := ur.Repo.GetAllEmployees(ctx)
	if err != nil {
		_ = middleware.HTTPError(w, r, http.StatusConflict, err.Error())
		return
	}

	if len(users) < 1 {
		_ = middleware.JSONMessages(w, r, http.StatusNoContent, "No se encontró el empleado")
		return
	}

	_ = middleware.JSON(w, r, http.StatusOK, users)
}