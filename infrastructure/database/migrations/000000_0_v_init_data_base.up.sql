-- CREATE

CREATE TABLE IF NOT EXISTS "employee" (
    "id"                uuid            NOT NULL,
    "first_name"        varchar(20)     NOT NULL,
    "other_name"        varchar(50),
    "first_surname"     varchar(20)     NOT NULL,
    "second_surname"    varchar(20),
    "job_country"       varchar(20)     NOT NULL,
    "type_ID"           varchar(20)     NOT NULL,
    "number_ID"         varchar(20)     NOT NULL UNIQUE,
    "email"             varchar(300)    NOT NULL UNIQUE,
    "entry_date"        date  NOT NULL,
    "area"              varchar(50)     NOT NULL,
    "status"            varchar(15)     NOT NULL,
    "created_at"        timestamp(50)  NOT NULL,
    "updated_at"        timestamp(50)  NOT NULL,
    PRIMARY KEY ("id")
    );

ALTER TABLE "employee" OWNER to postgres;

-- INSERTS
INSERT INTO public.employee (id,first_name,other_name,first_surname,second_surname,job_country,"type_ID","number_ID",email,entry_date,area,status,created_at,updated_at) VALUES
('f830bae7-18ad-47e8-b40c-c66766f654b0'::uuid,'ANA','','SEDAN','SEDAN','Colombia','Cédula de Ciudadanía','1114587135','ana.sedan.2@cidenet.com.co','2021-10-04','Compras','activo','2021-04-10 22:36:13','2021-04-10 22:36:13'),
('499f94df-3fae-4994-8820-d12333280b8d'::uuid,'ANA','','SEDAN','SEDAN','Estados Unidos','Cédula de Ciudadanía','1114587139','ana.sedan.3@cidenet.com.us','2021-10-04','Compras','activo','2021-04-10 22:36:34','2021-04-10 22:36:34'),
('d8cc3d96-bf77-4787-b5c5-6e4353d5b205'::uuid,'ANA','MARIA','BERNAL','ROA','Colombia','Cédula de Ciudadanía','1119000125','ana.bernal@cidenet.com.co','2021-10-04','Compras','activo','2021-04-10 21:23:34','2021-04-11 01:02:35.562411'),
('bb019913-e838-469e-842c-1733cb322467'::uuid,'JUAN','MANUEL','SANCLEMENTE','VELEZ','Colombia','Cédula de Ciudadanía','1119000222','juan.sanclemente@cidenet.com.co','2021-10-04','Infraestructura','activo','2021-04-11 01:13:09','2021-04-11 01:13:09'),
('5ced5abb-eca6-4f21-bfdf-62bc483e0a83'::uuid,'JUAN','MANUEL','SANCLEMENTE','VELEZ','Estados Unidos','Cédula de Ciudadanía','1119000789','juan.sanclemente.2@cidenet.com.us','2021-09-04','Administración','activo','2021-04-11 01:13:54','2021-04-11 01:13:54'),
('c69eef5d-dd3d-470b-a715-52686282aa71'::uuid,'DANIELA','','MARIN','BARRIOS','Colombia','Cédula de Ciudadanía','1119000145','daniela.marin@cidenet.com.co','2021-09-04','Infraestructura','activo','2021-04-11 01:15:52','2021-04-11 01:15:52'),
('9fa4687f-3845-45ea-b2af-b613329ff9f9'::uuid,'DANIELA','','MARIN','BARRIOS','Colombia','Cédula de Ciudadanía','1119000148','daniela.marin.2@cidenet.com.co','2021-09-04','Infraestructura','activo','2021-04-11 01:16:57','2021-04-11 01:16:57'),
('995bf247-2119-4420-b448-4546e9244359'::uuid,'DANIELA','','GARCIA','REYES','Colombia','Cédula de Ciudadanía','1119000108','daniela.garcia@cidenet.com.co','2021-09-04','Infraestructura','activo','2021-04-11 01:17:28','2021-04-11 01:17:28'),
('e0ef1fbe-ed41-49b6-a4cf-46190b2e252a'::uuid,'DANIELA','','GARCIA','REYES','Colombia','Cédula de Ciudadanía','111900017','daniela.garcia.2@cidenet.com.co','2021-09-04','Infraestructura','activo','2021-04-11 01:17:34','2021-04-11 01:17:34'),
('fa3558aa-c092-40d0-a4d2-cf550e008b16'::uuid,'SANTIAGO','','GARCIA','REYES','Colombia','Cédula de Ciudadanía','1119000167','santiago.garcia@cidenet.com.co','2021-09-04','Infraestructura','activo','2021-04-11 01:18:07','2021-04-11 01:18:07');
INSERT INTO public.employee (id,first_name,other_name,first_surname,second_surname,job_country,"type_ID","number_ID",email,entry_date,area,status,created_at,updated_at) VALUES
('7337b9da-5f09-4eb2-a4e2-68464473ef88'::uuid,'ANDRES','','PEREZ','REYES','Colombia','Cédula de Ciudadanía','1119000163','andres.perez@cidenet.com.co','2021-09-04','Infraestructura','activo','2021-04-11 01:18:29','2021-04-11 01:18:29'),
('038d6858-a3d4-4fb5-b9d5-0099a79c9f61'::uuid,'ANDRES','','PEREZ','REYES','Colombia','Cédula de Ciudadanía','1119000144','andres.perez.2@cidenet.com.co','2021-09-04','Compras','activo','2021-04-11 01:18:49','2021-04-11 01:18:49'),
('1de2b102-0116-4812-90c4-22275b32c182'::uuid,'WILLIAM','','CADAVID','ARCE','Colombia','Cédula de Ciudadanía','1119000122','william.cadavid@cidenet.com.co','2021-09-04','Compras','activo','2021-04-11 01:19:33','2021-04-11 01:19:33');