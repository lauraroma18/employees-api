package infrastructure

import (
	v1Employee "employees-api/domain/user/application/v1"
	"employees-api/infrastructure/database"
	"github.com/go-chi/chi"
	"net/http"
)

// Routes returns the API V1 Handler with configuration.
func Routes(conn *database.Data) http.Handler {
	router := chi.NewRouter()

	ur := v1Employee.NewEmployeeHandler(conn)
	router.Mount("/employees", routesEmployee(ur))

	return router
}

// routesEmployee returns employee router with each endpoint.
func routesEmployee(handler *v1Employee.EmployeeRouter) http.Handler {
	router := chi.NewRouter()

	router.Post("/", handler.CreateEmployeeHandler)
	router.Get("/", handler.GetAllEmployeesHandler)
	router.Get("/{employee_id}", handler.GetOneEmployeeHandler)
	router.Patch("/{employee_id}", handler.UpdateEmployeeHandler)

	return router
}

