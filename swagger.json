{
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "schemes": [
    "http",
    "https"
  ],
  "swagger": "2.0",
  "info": {
    "description": "The purpose of this application is to provide service for create, update and consult employees\n\n\nThis should show the struct of endpoints",
    "title": "Employees API.",
    "contact": {
      "url": "https://www.linkedin.com/in/lauraroma18/"
    },
    "version": "1.0.0"
  },
  "host": "localhost:8888",
  "basePath": "/api/v1",
  "paths": {
    "/employees": {
      "get": {
        "description": "Return all employees in the database",
        "produces": [
          "application/json"
        ],
        "schemes": [
          "http",
          "https"
        ],
        "tags": [
          "Employee"
        ],
        "summary": "GetAllEmployeesHandler.",
        "operationId": "getAllEmployees",
        "responses": {
          "200": {
            "$ref": "#/responses/SwaggerAllEmployeeResponse"
          },
          "204": {
            "$ref": "#/responses/SwaggerSuccessfullyMessage"
          },
          "409": {
            "$ref": "#/responses/SwaggerErrorMessage"
          }
        }
      },
      "post": {
        "description": "Create a new employee",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "schemes": [
          "http",
          "https"
        ],
        "tags": [
          "Employee"
        ],
        "summary": "CreateEmployeeHandler.",
        "operationId": "createEmployeeRequest",
        "parameters": [
          {
            "name": "Body",
            "in": "body",
            "schema": {
              "type": "object",
              "required": [
                "first_name",
                "first_surname",
                "job_country",
                "type_ID",
                "number_ID",
                "entry_date",
                "area"
              ],
              "properties": {
                "area": {
                  "type": "string",
                  "x-go-name": "Area"
                },
                "entry_date": {
                  "type": "string",
                  "x-go-name": "EntryDate"
                },
                "first_name": {
                  "type": "string",
                  "x-go-name": "FirstName"
                },
                "first_surname": {
                  "type": "string",
                  "x-go-name": "FirstSurname"
                },
                "job_country": {
                  "type": "string",
                  "x-go-name": "JobCountry"
                },
                "number_ID": {
                  "type": "string",
                  "x-go-name": "NumberID"
                },
                "other_name": {
                  "type": "string",
                  "x-go-name": "OtherName"
                },
                "second_surname": {
                  "type": "string",
                  "x-go-name": "SecondSurname"
                },
                "type_ID": {
                  "type": "string",
                  "x-go-name": "TypeID"
                }
              }
            }
          }
        ],
        "responses": {
          "201": {
            "$ref": "#/responses/SwaggerBasicEmployeeData"
          },
          "400": {
            "$ref": "#/responses/SwaggerErrorMessage"
          },
          "409": {
            "$ref": "#/responses/SwaggerErrorMessage"
          },
          "422": {
            "$ref": "#/responses/SwaggerErrorsMessage"
          }
        }
      }
    },
    "/employees/{employee_id}": {
      "get": {
        "description": "Return one employee by id",
        "produces": [
          "application/json"
        ],
        "schemes": [
          "http",
          "https"
        ],
        "tags": [
          "Employee"
        ],
        "summary": "GetOneEmployeeHandler.",
        "operationId": "getOneEmployeeRequest",
        "parameters": [
          {
            "type": "string",
            "name": "ID",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/SwaggerEmployeeResponse"
          },
          "204": {
            "$ref": "#/responses/SwaggerSuccessfullyMessage"
          },
          "400": {
            "$ref": "#/responses/SwaggerErrorMessage"
          },
          "409": {
            "$ref": "#/responses/SwaggerErrorMessage"
          },
          "422": {
            "$ref": "#/responses/SwaggerErrorsMessage"
          }
        }
      },
      "patch": {
        "description": "Update the employee by one id given",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "schemes": [
          "http",
          "https"
        ],
        "tags": [
          "Employee"
        ],
        "summary": "UpdateEmployeeHandler.",
        "operationId": "updateEmployeeRequest",
        "parameters": [
          {
            "type": "string",
            "name": "EmployeeID",
            "in": "path",
            "required": true
          },
          {
            "name": "Body",
            "in": "body",
            "schema": {
              "type": "object",
              "required": [
                "first_name",
                "first_surname",
                "job_country",
                "type_ID",
                "number_ID",
                "status",
                "area"
              ],
              "properties": {
                "area": {
                  "type": "string",
                  "x-go-name": "Area"
                },
                "first_name": {
                  "type": "string",
                  "x-go-name": "FirstName"
                },
                "first_surname": {
                  "type": "string",
                  "x-go-name": "FirstSurname"
                },
                "job_country": {
                  "type": "string",
                  "x-go-name": "JobCountry"
                },
                "number_ID": {
                  "type": "string",
                  "x-go-name": "NumberID"
                },
                "other_name": {
                  "type": "string",
                  "x-go-name": "OtherName"
                },
                "second_surname": {
                  "type": "string",
                  "x-go-name": "SecondSurname"
                },
                "status": {
                  "type": "string",
                  "x-go-name": "Status"
                },
                "type_ID": {
                  "type": "string",
                  "x-go-name": "TypeID"
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/SwaggerBasicEmployeeData"
          },
          "400": {
            "$ref": "#/responses/SwaggerErrorMessage"
          },
          "409": {
            "$ref": "#/responses/SwaggerErrorMessage"
          },
          "422": {
            "$ref": "#/responses/SwaggerErrorsMessage"
          }
        }
      }
    }
  },
  "definitions": {
    "Employee": {
      "description": "Data of Employee",
      "type": "object",
      "properties": {
        "area": {
          "type": "string",
          "x-go-name": "Area"
        },
        "created_at": {
          "type": "string",
          "format": "date-time",
          "x-go-name": "CreatedAt"
        },
        "email": {
          "type": "string",
          "x-go-name": "Email"
        },
        "entry_date": {
          "type": "string",
          "x-go-name": "EntryDate"
        },
        "first_name": {
          "type": "string",
          "x-go-name": "FirstName"
        },
        "first_surname": {
          "type": "string",
          "x-go-name": "FirstSurname"
        },
        "id": {
          "type": "string",
          "x-go-name": "ID"
        },
        "job_country": {
          "type": "string",
          "x-go-name": "JobCountry"
        },
        "number_ID": {
          "type": "string",
          "x-go-name": "NumberID"
        },
        "other_name": {
          "type": "string",
          "x-go-name": "OtherName"
        },
        "second_surname": {
          "type": "string",
          "x-go-name": "SecondSurname"
        },
        "status": {
          "type": "string",
          "x-go-name": "Status"
        },
        "type_ID": {
          "type": "string",
          "x-go-name": "TypeID"
        },
        "updated_at": {
          "type": "string",
          "format": "date-time",
          "x-go-name": "UpdatedAt"
        }
      },
      "x-go-package": "employees-api/domain/user/domain/model"
    },
    "EmployeeResponse": {
      "type": "object",
      "properties": {
        "area": {
          "type": "string",
          "x-go-name": "Area"
        },
        "created_at": {
          "type": "string",
          "format": "date-time",
          "x-go-name": "CreatedAt"
        },
        "email": {
          "type": "string",
          "x-go-name": "Email"
        },
        "entry_date": {
          "type": "string",
          "x-go-name": "EntryDate"
        },
        "first_name": {
          "type": "string",
          "x-go-name": "FirstName"
        },
        "first_surname": {
          "type": "string",
          "x-go-name": "FirstSurname"
        },
        "id": {
          "type": "string",
          "x-go-name": "ID"
        },
        "job_country": {
          "type": "string",
          "x-go-name": "JobCountry"
        },
        "number_ID": {
          "type": "string",
          "x-go-name": "NumberID"
        },
        "other_name": {
          "type": "string",
          "x-go-name": "OtherName"
        },
        "second_surname": {
          "type": "string",
          "x-go-name": "SecondSurname"
        },
        "status": {
          "type": "string",
          "x-go-name": "Status"
        },
        "type_ID": {
          "type": "string",
          "x-go-name": "TypeID"
        },
        "updated_at": {
          "type": "string",
          "format": "date-time",
          "x-go-name": "UpdatedAt"
        }
      },
      "x-go-package": "employees-api/domain/user/application/v1/response"
    },
    "ErrorMessage": {
      "description": "ErrorMessage structure that returns errors",
      "type": "object",
      "properties": {
        "message": {
          "type": "string",
          "x-go-name": "Message"
        },
        "status": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "Status"
        }
      },
      "x-go-package": "employees-api/infrastructure/middleware"
    },
    "ErrorsMessage": {
      "description": "ErrorsMessage structure that returns a group of errors",
      "type": "object",
      "properties": {
        "message": {
          "type": "object",
          "x-go-name": "Message"
        },
        "status": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "Status"
        }
      },
      "x-go-package": "employees-api/infrastructure/middleware"
    },
    "SuccessfullyMessage": {
      "description": "SuccessfullyMessage structure that returns successfully",
      "type": "object",
      "properties": {
        "message": {
          "type": "string",
          "x-go-name": "Message"
        },
        "status": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "Status"
        }
      },
      "x-go-package": "employees-api/infrastructure/middleware"
    },
    "UserResponse": {
      "type": "object",
      "properties": {
        "email": {
          "type": "string",
          "x-go-name": "Email"
        },
        "first_name": {
          "type": "string",
          "x-go-name": "FirstName"
        },
        "first_surname": {
          "type": "string",
          "x-go-name": "FirstSurname"
        },
        "id": {
          "type": "string",
          "x-go-name": "ID"
        }
      },
      "x-go-package": "employees-api/domain/user/application/v1/response"
    }
  },
  "responses": {
    "SwaggerAllEmployeeResponse": {
      "description": "EmployeeResponse contains all employee data",
      "schema": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/EmployeeResponse"
        }
      }
    },
    "SwaggerBasicEmployeeData": {
      "description": "SwaggerBasicEmployeeData contains basic data about employee",
      "schema": {
        "$ref": "#/definitions/UserResponse"
      }
    },
    "SwaggerEmployeeResponse": {
      "description": "EmployeeResponse contains all employee data",
      "schema": {
        "$ref": "#/definitions/EmployeeResponse"
      }
    },
    "SwaggerErrorMessage": {
      "description": "ErrorMessage standardized error response.",
      "schema": {
        "$ref": "#/definitions/ErrorMessage"
      }
    },
    "SwaggerErrorsMessage": {
      "description": "ErrorsMessage structure that returns a group of errors.",
      "schema": {
        "$ref": "#/definitions/ErrorsMessage"
      }
    },
    "SwaggerMap": {
      "description": "Map is a convenient way to create objects of unknown types.",
      "schema": {
        "type": "object",
        "additionalProperties": {
          "type": "object"
        }
      }
    },
    "SwaggerSuccessfullyMessage": {
      "description": "SuccessfullyMessage structure that returns successfully",
      "schema": {
        "$ref": "#/definitions/SuccessfullyMessage"
      }
    }
  }
}